//package com.authenticationservice;
//
//import io.restassured.http.ContentType;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.MethodOrderer;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.TestMethodOrder;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.web.server.LocalServerPort;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.test.web.reactive.server.WebTestClient;
//import org.springframework.web.reactive.function.BodyInserters;
//import org.testcontainers.junit.jupiter.Testcontainers;
//import reactor.core.publisher.Mono;
//
//import java.time.Duration;
//
//import static io.restassured.module.webtestclient.RestAssuredWebTestClient.given;
//
//
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@Testcontainers
//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
//public class UserIntegrationTest {
//
//  private String users = "/users";
//
//  private String uri;
////  @Autowired
//  private final WebTestClient webTestClient = WebTestClient
//      .bindToServer()
//      .baseUrl(uri)
//      .responseTimeout(Duration.ofMinutes(1L))
//      .build();
//
////  @Autowired
////  private UserService userService;
//
//  @LocalServerPort
//  private int port;
//
//  @BeforeEach
//  public void initialiseRestAssuredMockMvcStandalone() {
//    uri = "http://localhost:" + port;
//  }
//
//  @Test
//  void createUserTest() {
//
//    var createUser = new CreateUserDto(
//        "alex.cohen@weareadaptive.com",
//        "password",
//        "alex",
//        "cohen"
//    );
//
//    var createdUserResponse = new UserResponse(
//        1,
//        "alex.cohen@weareadaptive.com",
//        "alex",
//        "cohen"
//    );
////    //@formatter:off
////    given()
////        .standaloneSetup(webTestClient.mutate())
////        .contentType(ContentType.JSON)
////        .body(createUser)
////    .when()
////        .post("/users")
////    .then()
////        .statusCode(HttpStatus.CREATED.value())
////            .
////    //@formatter:on
//
//    webTestClient
//        .post()
//        .uri(users)
//        .contentType(MediaType.APPLICATION_JSON)
//        .body(Mono.just(createUser),CreateUserDto.class)
//        .exchange()
//        .expectStatus()
//        .isEqualTo(HttpStatus.CREATED.value())
//        .expectBody(UserResponse.class)
//        .isEqualTo(createdUserResponse);
//  }
//
//}
