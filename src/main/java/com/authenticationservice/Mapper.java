package com.authenticationservice;

public class Mapper {
  public static UserResponse map(Users user) {
    return new UserResponse(user.getId(), user.getEmail(),user.getFirstName(), user.getLastName());
  }
}
