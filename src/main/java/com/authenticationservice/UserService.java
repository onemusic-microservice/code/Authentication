package com.authenticationservice;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import reactor.core.publisher.Mono;

@Service
public class UserService {
  private final UserRepository userRepository;


  public UserService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

//  public Mono<UserResponse> createUser(CreateUserDto createUser) {
  public Mono<Users> createUser(CreateUserDto createUser) {
    System.out.println(createUser);
    var user = Users
        .builder()
        .email(createUser.email())
        .pass(createUser.password())
        .firstName(createUser.firstName())
        .lastName(createUser.lastName())
        .build();
    System.out.println(user);
//    System.out.println(userRepository.save(user).block());
    return userRepository
        .save(user);
//        .map(Mapper::map);
  }

}
