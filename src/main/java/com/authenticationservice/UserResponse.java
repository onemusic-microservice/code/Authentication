package com.authenticationservice;

public record UserResponse (
    long id,
    String email,
    String firstName,
    String lastName
)
{}
