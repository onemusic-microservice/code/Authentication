//package com.authenticationservice;
//
//import io.r2dbc.postgresql.PostgresqlConnectionConfiguration;
//import io.r2dbc.postgresql.PostgresqlConnectionFactory;
//import io.r2dbc.spi.ConnectionFactory;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.r2dbc.repository.support.R2dbcRepositoryFactory;
//import org.springframework.data.relational.core.mapping.RelationalMappingContext;
//import org.springframework.r2dbc.core.DatabaseClient;
//
//@Configuration
//public class DatabaseConfig {
//  @Bean
//  PostgresqlConnectionFactory connectionFactory() {
//    return new PostgresqlConnectionFactory(
//        PostgresqlConnectionConfiguration
//            .builder()
//            .host("localhost")
//            .database("authentication")
//            .password("password")
//            .build()
//    );
//  }
//  @Bean
//  DatabaseClient databaseClient(ConnectionFactory connectionFactory) {
//    return DatabaseClient
//        .builder()
//        .connectionFactory(connectionFactory)
//        .build();
//  }
//
//  @Bean
//  R2dbcRepositoryFactory repositoryFactory(DatabaseClient databaseClient) {
//    RelationalMappingContext context = new RelationalMappingContext();
//    context.afterPropertiesSet();
//    return new R2dbcRepositoryFactory(databaseClient,context);
//  }
//  @Bean
//}
