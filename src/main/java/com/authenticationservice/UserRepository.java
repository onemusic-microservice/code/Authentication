package com.authenticationservice;

import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
//public interface UserRepository extends ReactiveCrudRepository<User,Long> {
public interface UserRepository extends R2dbcRepository<Users,Long> {

  Mono<Boolean> existsById(Long aLong);
}
