package com.authenticationservice;

public record CreateUserDto (
    String email,
    String password,
    String firstName,
    String lastName
)
{}
